<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {   
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if($this->form_validation->run() == false){
        $data['title'] = 'Login page';
        $this->load->view('templates/auth_header');
        $this->load->view('auth/login');
        $this->load->view('templates/auth_footer');
        }else{
            $this->_login();
        }
    }

    private function _login(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $masyarakat = $this->db->get_where('masyarakat', ['username'=> $username])->row_array();
        
        // jika masyarakatnya ada
        if($masyarakat) {
            //jika masyarakatnya aktif
            
                //cek password
                if (password_verify($password, $masyarakat['password'])) {
                    $data = [
                        'username' => $masyarakat['username'],
                        'nik'  => $masyarakat['nik'],
                    ];
                    $this->session->set_userdata($data);
                    redirect('masyarakat');
                }else{
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">password salah!</div>');
                    redirect('auth');
                }
        }else{
            $this->session->set_flashdata('message', '<div class="alert alert-danger">username tidak terdaftar</div>');
            redirect('auth');
        }
    }
    public function registration()
    {
        $this->form_validation->set_rules('nik', 'Nik', 'required|trim|is_unique[masyarakat.nik]',[
            'is_unique' => 'Nik sudah terdaftar'
        ]);    
        
        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('telp', 'Telp', 'required|trim');
        $this->form_validation->set_rules('username', 'Usernaname', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');

        if($this->form_validation->run() == false){
           $data['title'] = 'User Registration';
           $this->load->view('templates/auth_header', $data);
           $this->load->view('auth/registration');
           $this->load->view('templates/auth_footer');
        } else {
            $data = [
                'nik' => htmlspecialchars($this->input->post('nik')),
                'nama' => htmlspecialchars($this->input->post('nama')),
                'telp' => $this->input->post('telp'),
                'username' => ($this->input->post('username')),
                'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT)

                
            ];

            $this->db->insert('masyarakat', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">SELAMAT!anda sudah terdaftar silahkan login</div>');
            redirect('auth');}


        
    }
    public function dashboard()
    {
        $this->load->view('templates/auth_header');
        $this->load->view('auth/dashboard');
        $this->load->view('templates/auth_footer');
    }

    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('password');

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">ANDA TELAH LOGOUT!</div>');
            redirect('auth');}


    }






