<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Masyarakat extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('auth_model');
        $this->load->model('profile_model');
        $this->load->model('Pengaduan_m');
    }

       public function index() 
    {

        $data['title'] = 'pengaduan masyarakat'; 
        $data['masyarakat'] = $this->db->get_where('masyarakat', ['username' =>
        $this->session->userdata('username')])->row_array();

        $this->load->view('user/index', $data);
    }
    public function profile()
    {
        $data['title'] = 'Profile';

        $data['masyarakat'] = $this->db->get_where('masyarakat', ['username' =>
        $this->session->userdata('username')])->row_array();

        $this->load->view('templates/auth_header');
        $this->load->view('templates/auth_footer');
        $this->load->view('user/profile_utama', $data);
    }
    public function change_image()
    {
        $data['title'] = 'Profile';

        $data['masyarakat'] = $this->db->get_where('masyarakat', ['username' =>
        $this->session->userdata('username')])->row_array();

        $this->load->view('templates/auth_header');
        $this->load->view('templates/auth_footer');
        $this->load->view('user/profile', $data);
    }
    public function upload_image()
	{
		$this->load->model('profile_model');
        $this->load->model('auth_model');
		$data['current_user'] = $this->auth_model->current_user();

		if ($this->input->method() === 'post') {
			// the user id contain dot, so we must remove it
			$file_name = str_replace('.','',$data['current_user']->nik);
			$config['upload_path']          = realpath(APPPATH . '../assets/uploads/');
			$config['allowed_types']        = 'gif|jpg|jpeg|png';
			$config['max_size']             = 2048;
			$config['remove_spaces']        = TRUE;
			$config['detect_mime']        	= TRUE;
			$config['mod_mime_fix']        	= TRUE;
			$config['encrypt_name']        	= TRUE;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('avatar')) {
				$data['error'] = $this->upload->display_errors();
				die($this->upload->display_errors());
			} else {
				$uploaded_data = $this->upload->data();

				$new_data = [
					'nik' => $data['current_user']->nik,
					'image' => $uploaded_data['file_name'],
				];
	
				if ($this->profile_model->update($new_data)) {
					$this->session->set_flashdata('message', 'Avatar was updated');
					redirect(site_url('masyarakat/profile'));
				}
			}
		}
    }
    public function ganti_password()
    {
        $data['title'] = '';
        $this->load->library('form_validation');
        $this->load->model('profile_model');
        $data['current_user'] = $this->auth_model->current_user();

        if ($this->input->method() === 'post') {
            $rules = $this->profile_model->password_rules();
            $this->form_validation->set_rules($rules);

            if ($this->form_validation->run() === FALSE) {
                return $this->load->view('user/ganti_password.php', $data);
            }

            $new_password_data = [
                'nik' => $data['current_user']->nik,
                'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            ];

            if ($this->profile_model->update($new_password_data)) {
                $this->session->set_flashdata('message', 'Password was changed');
                redirect(site_url('masyarakat/ganti_password'));
            }
        }

        $this->load->view('user/ganti_password.php', $data);
    }

    public function remove_image()
	{
		$current_user = $this->auth_model->current_user();
		$this->load->model('auth_model');
		
		// hapus file
		$file_name = str_replace('.', '', $current_user->id);
		array_map('unlink', glob(FCPATH."/upload/avatar/$file_name.*"));

		// set avatar menjadi null
		$new_data = [
			'nik' => $current_user->nik,
			'image' => null,
		];

		if ($this->profile_model->update($new_data)) {
			$this->session->set_flashdata('message', 'Avatar dihapus!');
			redirect(site_url('masyarakat/profile'));
		}
	}
    public function pengaduan()
	{
		$data['title'] = 'Pengaduan';
		$masyarakat = $this->db->get_where('masyarakat',['username' => $this->session->userdata('username')])->row_array();
		$data['data_pengaduan'] = $this->Pengaduan_m->data_pengaduan_masyarakat_nik($masyarakat['nik'])->result_array();
		
		$this->form_validation->set_rules('isi_laporan','Isi Laporan Pengaduan','trim|required');
		$this->form_validation->set_rules('foto','Foto Pengaduan','trim');
		print_r($this->input->post('isi_laporan'));
		if ($this->input->post('isi_laporan') == null) :
            $this->load->view('templates/auth_header');
            $this->load->view('templates/auth_footer');
            $this->load->view('user/pengaduan', $data);
		else :
			$upload_foto = $this->upload_foto('foto'); // parameter nama foto
			if ($upload_foto == FALSE) :
				$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
					Upload foto pengaduan gagal, hanya png,jpg dan jpeg yang dapat di upload!
					</div>');

				redirect('Masyarakat/Pengaduan');
			else :

				$params = [
					'tgl_pengaduan'  	=> date('Y-m-d'),
					'nik'				=> $masyarakat['nik'],
					'isi_laporan'		=> htmlspecialchars($this->input->post('isi_laporan',true)),
					'foto'				=> $upload_foto,
					'status'			=> '0',
				];

				$resp = $this->Pengaduan_m->create($params);

				if ($resp) :
					$this->session->set_flashdata('message','<div class="alert alert-primary" role="alert">
						Laporan berhasil dibuat
						</div>');

					redirect('Masyarakat/Pengaduan');
				else :
					$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
						Laporan gagal dibuat!
						</div>');

					redirect('Masyarakat/Pengaduan');
				endif;

			endif;
		endif;
	}
	public function create_pengaduan()
	{
		$upload_foto = $this->upload_foto('foto'); // parameter nama foto
		$masyarakat = $this->db->get_where('masyarakat',['username' => $this->session->userdata('username')])->row_array();
		$params = [
			'tgl_pengaduan'  	=> date('Y-m-d'),
			'nik'				=> $masyarakat['nik'],
			'isi_laporan'		=> htmlspecialchars($this->input->post('isi_laporan',true)),
			'foto'				=> $upload_foto,
			'status'			=> '0',
		];

		$resp = $this->Pengaduan_m->create($params);

		if ($resp) :
			$this->session->set_flashdata('message','<div class="alert alert-primary" role="alert">
				Laporan berhasil dibuat
				</div>');

			redirect('Masyarakat/Pengaduan');
		else :
			$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
				Laporan gagal dibuat!
				</div>');

			redirect('Masyarakat/Pengaduan');
		endif;
		
			
	}
    public function pengaduan_baru()
	{
		$data['title'] = 'Pengaduan';
		$masyarakat = $this->db->get_where('masyarakat',['username' => $this->session->userdata('username')])->row_array();
		$data['data_pengaduan'] = $this->Pengaduan_m->data_pengaduan_masyarakat_nik($masyarakat['nik'])->result_array();
		
		$this->form_validation->set_rules('isi_laporan','Isi Laporan Pengaduan','trim|required');
		$this->form_validation->set_rules('foto','Foto Pengaduan','trim');

		if ($this->form_validation->run() == FALSE) :
            $this->load->view('templates/auth_header');
            $this->load->view('templates/auth_footer');
            $this->load->view('user/pengaduan_baru', $data);
		else :
			$upload_foto = $this->upload_foto('foto'); // parameter nama foto
			if ($upload_foto == FALSE) :
				$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
					Upload foto pengaduan gagal, hanya png,jpg dan jpeg yang dapat di upload!
					</div>');

				redirect('Masyarakat/Pengaduan');
			else :

				$params = [
					'tgl_pengaduan'  	=> date('Y-m-d'),
					'nik'				=> $masyarakat['nik'],
					'isi_laporan'		=> htmlspecialchars($this->input->post('isi_laporan',true)),
					'foto'				=> $upload_foto,
					'status'			=> '0',
				];

				$resp = $this->Pengaduan_m->create($params);

				if ($resp) :
					$this->session->set_flashdata('message','<div class="alert alert-primary" role="alert">
						Laporan berhasil dibuat
						</div>');

					redirect('Masyarakat/Pengaduan');
				else :
					$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
						Laporan gagal dibuat!
						</div>');

					redirect('Masyarakat/Pengaduan');
				endif;

			endif;
		endif;
	}

	public function pengaduan_detail($id)
	{

		$cek_data = $this->db->get_where('pengaduan',['id_pengaduan' => htmlspecialchars($id)])->row_array();

		if ( ! empty($cek_data)) :

			$data['title'] = 'Detail Pengaduan';

			$data['data_pengaduan'] = $this->Pengaduan_m->data_pengaduan_tanggapan(htmlspecialchars($id))->row_array();
			if ($data['data_pengaduan']) :
			$this->load->view('templates/auth_header');
            $this->load->view('templates/auth_footer');
            $this->load->view('user/pengaduan_detail', $data);
			else :
				$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
					Pengaduan sedang di proses!
					</div>');

				redirect('Masyarakat/Pengaduan');			
			endif;
			
		else :
			$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
				data tidak ada
				</div>');

			redirect('Masyarakat/Pengaduan');			
		endif;
	}

	public function pengaduan_batal($id)
	{
		$cek_data = $this->db->get_where('pengaduan',['id_pengaduan' => htmlspecialchars($id)])->row_array();

		if ( ! empty($cek_data)) :

			if ($cek_data['status'] == '0') :

				$resp = $this->db->delete('pengaduan',['id_pengaduan' => $id]);

				// hapus file
				$path = './assets/uploads/'.$cek_data['foto'];
				unlink($path);

				if ($resp) :
					$this->session->set_flashdata('message','<div class="alert alert-primary" role="alert">
						Hapus pengaduan berhasil
						</div>');

					redirect('Masyarakat/Pengaduan');
				else :
					$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
						Hapus pengaduan gagal!
						</div>');

					redirect('Masyarakat/Pengaduan');
				endif;

			else :
				$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
					Pengaduan sedang di proses!
					</div>');

				redirect('Masyarakat/Pengaduan');
			endif;

		else :
			$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
				data tidak ada
				</div>');

			redirect('Masyarakat/Pengaduan');				
		endif;
	}

	public function edit($id)
	{
		$cek_data = $this->db->get_where('pengaduan',['id_pengaduan' => htmlspecialchars($id)])->row_array();

		if ( ! empty($cek_data)) :

			if ($cek_data['status'] == '0') :

				$data['title'] = 'Edit Pengaduan';
				$data['pengaduan'] = $cek_data;

				$this->form_validation->set_rules('isi_laporan','Isi Laporan Pengaduan','trim|required');
				$this->form_validation->set_rules('foto','Foto Pengaduan','trim');
				
				if ($this->form_validation->run() == FALSE) :
                    $this->load->view('templates/auth_header');
                    $this->load->view('templates/auth_footer');
                    $this->load->view('user/edit_pengaduan', $data);
				else :

					$upload_foto = $this->upload_foto('foto'); // parameter nama foto

					if ($upload_foto == FALSE) :
						$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
							Upload foto pengaduan gagal, hanya png,jpg dan jpeg yang dapat di upload!
							</div>');

						redirect('Masyarakat/Pengaduan');
					else :

						// hapus file
						$path = './assets/uploads/'.$cek_data['foto'];
						unlink($path);

						$params = [
							'isi_laporan'		=> htmlspecialchars($this->input->post('isi_laporan',true)),
							'foto'				=> $upload_foto,
						];

						$resp = $this->db->update('pengaduan',$params,['id_pengaduan' => $id]);;

						if ($resp) :
							$this->session->set_flashdata('message','<div class="alert alert-primary" role="alert">
								Laporan berhasil dibuat
								</div>');

							redirect('Masyarakat/Pengaduan');
						else :
							$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
								Laporan gagal dibuat!
								</div>');

							redirect('Masyarakat/Pengaduan');
						endif;

					endif;

				endif;

			else :
				$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
					Pengaduan sedang di proses!
					</div>');

				redirect('Masyarakat/Pengaduan');
			endif;

		else :
			$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
				data tidak ada
				</div>');

			redirect('Masyarakat/Pengaduan');				
		endif;
	}

	private function upload_foto($foto)
	{
		$config['upload_path']          = realpath(APPPATH . '../assets/uploads');
		$config['allowed_types']        = 'gif|jpg|jpeg|png';
		$config['max_size']             = 2048;
		$config['remove_spaces']        = TRUE;
		$config['detect_mime']        	= TRUE;
		$config['mod_mime_fix']        	= TRUE;
		$config['encrypt_name']        	= TRUE;
		
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload($foto)) :
			
			return FALSE;
		else :
			return $this->upload->data('file_name');
		endif;
	}
}

